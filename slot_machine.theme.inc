<?php

/*********** Form theme functions ***********/

/*
 * Theme the content queue form as a table
 */
function theme_slot_machine_content_queue_form($form) {
  $output .= drupal_render($form['include_home']);
  foreach (element_children($form['slot_actions']) as $key) {
    $row = array();
    $row[0]['data'] = drupal_render($form['slot_actions'][$key]) . drupal_render($form['slot_topics']['cb'. $key]);
    $row[1]['data'] = drupal_render($form['head_deck'][$key]);
    $row[2]['data'] = drupal_render($form['author'][$key]);
    $row[3]['data'] = drupal_render($form['topics'][$key]);
    $row[4]['data'] = drupal_render($form['feature_type'][$key]);
    $row[5]['data'] = drupal_render($form['created'][$key]);
    $row[6]['data'] = drupal_render($form['history'][$key]);
    $rows[] = $row;
  }
  $output .= theme('table', $form['header']['#value'], $rows);
  $output .= drupal_render($form);
  return $output;
}

/*
 * Theme the schedule form as a table
 */
function theme_slot_machine_schedule_form($form) {
  if ($form['#parameters'][1]['storage']['step'] == 1) {
    foreach (element_children($form['slot_label']) as $key) {
      $row = array(
        'data' => array(
          array('data' => drupal_render($form['slot_label'][$key]) . drupal_render($form['slot_frequencies'][$key])),
          array(
            'data' => drupal_render($form['remove'][$key]) . drupal_render($form['rot'][$key]),
            'align' => 'center',
          ),
          array('data' => drupal_render($form['head_deck'][$key])),
          array('data' => drupal_render($form['author'][$key])),
          array('data' => drupal_render($form['topics'][$key])),
          array('data' => drupal_render($form['created'][$key])),
          array('data' => drupal_render($form['schedule'][$key])),
          array('data' => drupal_render($form['live'][$key])),
        ),
        'class' => (strpos($form['slot_label'][$key]['#value'], '<a href') === FALSE ? 'selected' : ''),
      );
      $rows[] = $row;
    }
    $output .= theme('table', $form['header']['#value'], $rows);
  }
  $output .= drupal_render($form);
  return $output;
}

/*
 * Theme the queue form as a table
 */
function theme_slot_machine_queue_form($form) {
  foreach (element_children($form['priority']) as $key) {
    $row = array();
    $row[0] = array(
      'data' => drupal_render($form['priority'][$key]),
      'align' => 'center',
    );
    $row[1] = array(
      'data' => drupal_render($form['remove'][$key]),
      'align' => 'center',
    );
    $row[2]['data'] = drupal_render($form['head_deck'][$key]);
    $row[3]['data'] = drupal_render($form['author'][$key]);
    $row[4]['data'] = drupal_render($form['topics'][$key]);
    $row[5]['data'] = drupal_render($form['created'][$key]);
    $row[6]['data'] = drupal_render($form['recent_activity'][$key]);
    $row[7]['data'] = drupal_render($form['schedule'][$key]);
    $rows[] = $row;
  }
  $output .= theme('table', $form['header']['#value'], $rows);
  $output .= drupal_render($form);
  return $output;
}

/*********** Display Slotted Nodes theme functions ***********/

function theme_slot_machine_slot_nodes($nids) {
  $output = '';
  foreach($nids as $name => $nid) {
    $output .= '<h2>'. $name .'</h2>';
    $node = node_load($nid);
    $output .= theme('node', $node, TRUE);
  }
  return $output;
}

function theme_slot_machine_all_topics($topic_nids) {
  $output = '';
  foreach ($topic_nids as $topic => $nid) {
    $output .= '<h2>'. $topic .'</h2>';
    $node = node_load($nid);
    $output .= theme('node', $node, TRUE);
  }
  return $output;
}

/*********** Content Feature Queue theme functions ***********/

function theme_slot_machine_slot_all_header() {
  return t('Slot This<span class="throbber">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><br/>(<a href="javascript:void(0)" class="slot-all">slot all</a>)');
}

function theme_slot_machine_head_deck_header() {
  return t('Head and Deck');
}

function theme_slot_machine_author_header() {
  return t('Author');
}

function theme_slot_machine_topic_header() {
  return t('Topic');
}

function theme_slot_machine_feature_type_header() {
  return t('Feature Type');
}

function theme_slot_machine_published_header() {
  return t('Published');
}

function theme_slot_machine_audit_trail_header() {
  return t('Audit Trail');
}

function theme_slot_machine_priority_header() {
  return t('Priority');
}

function theme_slot_machine_recent_activity_header() {
  return t('Recent Activity');
}

function theme_slot_machine_scheduled_header() {
  return t('Scheduled');
}

function theme_slot_machine_live_schedule_header() {
  return t('Live on Schedule');
}

function theme_slot_machine_per_page($topic, $type, $limit, $search) {
  $output = '<div id="results-per-page">' . t('Results per page: ');
  foreach (array(20, 50, 100) as $pages) {
    if ($limit == $pages) {
      $output .= $pages .' | ';
    }
    else {
      $name = $pages;
      $output .= l($name, 'admin/content/slotqueue/'. $topic .'/'. $type .'/'. $pages .'/'. $search) . ($pages ? ' | ' : '');
    }
  }
  $output = trim($output, ' | ') . '</div>';
  return $output;
}

function theme_slot_machine_slot_actions($nid) {
  $output = '<a class="item-select-all" name="'. $nid .'" href="javascript:void(0)">+</a>&nbsp;';
  $output .= '<a class="choose" name="'. $nid .'" href="javascript:void(0)">Choose</a>';
  return $output;
}

function theme_slot_machine_popup_prefix($nid) {
  return '<div class="slot-popup" id="'. $nid .'_slot_popup">';
}

function theme_slot_machine_popup_suffix($nid) {
  $output = '<button class="slot-popup-cancel" name="'. $nid .'" href="javascript:void(0)">Cancel</button> ';
  $output .= '<button class="slot-popup-ok" name="'. $nid .'" href="javascript:void(0)">OK</button>';
  $output .= '</div>';
  $output .= '<div id="'. $nid .'_slots"></div>'; // The HTML inside this div is inserted via jQuery
  return $output;
}

function theme_slot_machine_head_deck($head, $deck, $nid, $type) {
  $output .= l((strlen($head) < 101 ? $head : _sm_helper_truncate($head, 100)), 'node/'. $nid);
  if ($deck) {
    $output .= '<br/>'. (strlen($deck) < 101 ? $deck : _sm_helper_truncate($deck, 100));
  }
  return $output;
}

function theme_slot_machine_audit_trail($trail) {
  if (count($trail)) {
    foreach ($trail as $item) {
      $output .= $item .'<br/><br/>';
    }
  }
  else {
    $output = 'Never';
  }
  return $output;
}

function theme_slot_machine_topic_type_labels($topic, $type) {
  $output = '<h3>'. $topic .' | '. $type .'</h3>';
  return $output;
}

/*********** "Slot This" button theme functions ***********/

function theme_slot_machine_slot_this_button($nid) {
  return '<button class="slot-this-button" name="'. $nid .'" href="javascript:void(0)">Slot This</button> ';
}

/*********** Slot Machine Schedule theme functions ***********/

function theme_slot_machine_view_schedule_topic_label($topic) {
  $output = '<strong>'. $topic .'</strong><br/>';
  return $output;
}

function theme_slot_machine_schedule_header($preview = FALSE) {
  $output = ($preview ? strtoupper($preview) .' PREVIEW' : 'CURRENT') .' SCHEDULE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
  $output .= format_date(time(), 'custom', 'l, F j, Y g:i a');
  return $output;
}

function theme_slot_machine_slot_label($topic, $type, $name, $link, $rotatable) {
  if ($link) {
    $output = l($name, 'admin/content/slotmachine/'. $topic .'/'. $type);
  }
  else {
    $output = '<strong>'. $name .'</strong>';
  }
  if ($rotatable) {
    $output .= ' (R->'. $rotatable .')';
  }
  return $output;
}

function theme_slot_machine_rot_checkbox_prefix($sid) {
  return '<div class="rot-checkbox" id="rot-checkbox-'. $sid .'">';
}

function theme_slot_machine_rot_checkbox_suffix() {
  return '</div>';
}

function theme_slot_machine_recent_activity_item($topic, $date, $rot, $scheduled = FALSE) {
  $output = '<strong>';
  if ($scheduled) {
    $output .= '<em>Scheduled</em><br/>';
  }
  $output .= $topic .'<br/>'. $date . ($rot ? ' + Rot' : '');
  return $output;
}

function theme_slot_machine_preview_label($name, $frequency) {
  $output = '<strong>'. $name .'</strong>';
  $output .= '<br/>'. $frequency;
  return $output;
}

function theme_slot_machine_queue_header($type) {
  $output .= strtoupper($type) .' QUEUE';
  return $output;
}

function theme_slot_machine_select_all_header() {
  return t('Remove<br/>(<a href="javascript:void(0)" class="queue-select-all">select all</a>)');
}

function theme_slot_machine_reorder_confirmation_popup_prefix() {
  $output = '<div id="confirmation-popup">';
  $output .= t('Are you sure you want to remove the selected item(s) from the queue?') .'<br/><br/>';
  $output .= '<button id="confirmation-cancel" href="javascript:void(0)">Cancel</button> ';
  return $output;
}

function theme_slot_machine_reorder_confirmation_popup_suffix() {
  $output .= '</div>';
  return $output;
}