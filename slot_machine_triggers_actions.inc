<?php

/**
 * @file
 *  Triggers and actions supported by the Slot Machine module.
 */

/**
 * Implementation of hook_action_info_alter().
 */
function slot_machine_action_info_alter(&$info) {
  foreach (array_keys($info) as $key) {
    // Modify each action's hooks declaration, changing it to say 
    // that the action supports any hook.
    $info[$key]['hooks'] = 'any';
  }
}

/**
 * Implementation of hook_hook_info().
 */
function slot_machine_hook_info() {
  return array(
    'slot_machine' => array(
      'slot_machine' => array(
        'live' => array(
          'runs when' => t('When a post goes live on the schedule'),
        ),
      ),
    ),
  );
}

/**
 * Implementation of hook_slot_machine().
 *
 * Note confusing name due to fact that trigger name needs to equal module name.
 * @see slot_machine_hook_info()
 *
 * @param $op
 *   The operation trigger that determines which actions are triggered.
 * @param $node
 *   The node to pass in as context.
 */
function slot_machine_slot_machine($op, $node = NULL) {
  if (!module_exists("trigger")) {
    return;
  }
  $aids = _trigger_get_hook_aids('slot_machine', $op);
  if (empty($aids)) {
    return;
  }
  watchdog('slot_machine', '%op trigger is actioning "@aids"', array('%op' => $op, '@aids' => implode(', ', array_keys($aids))));
  
  global $user;
  $context = array('hook' => 'slot_machine', 'op' => $op, 'user' => $user);

  // We need to get the expected object if the action's type is not 'node'.
  foreach ($aids as $aid => $action_info) {
    if ($action_info['type'] != 'node') {
      if (!isset($objects[$action_info['type']])) {
        $object = _trigger_normalize_node_context($action_info['type'], $node);
        if (!$object) {
          $object = $node;
        }
      }
      // Since we know about the node, we pass that info along to the action.
      $context['node'] = $node;
      $result = actions_do($aid, $object, $context);
    }
    else {
      actions_do($aid, $node, $context);
    }
  }
}

/**
 * Implementation of hook_action_info().
 */
function slot_machine_action_info() {
  $actions = array();

  $actions['slot_machine_queue_action'] = array(
    'type' => 'node',
    'description' => t('Slot to end of slot machine queue'),
    'configurable' => TRUE,
    'hooks' => array(
      'nodeapi' => array('presave', 'insert', 'update'),
    ),
  );

  return $actions;
}

/**
 * Form for configurable action to slot node to end of slot machine queue.
 */
function slot_machine_queue_action_form($context) {
  $vocab = variable_get('slot_machine_topics', 'none');
  
  $options = array('all' => 'All queues associated with node\'s terms');
  if (variable_get('slot_machine_home_topic', 1)) {
    $options[0] = t('Home');      
  }
  $topics = _sm_get_vocabulary_terms($vocab);
  
  foreach ($topics as $topic_tid => $topic_name) {
    $options[$topic_tid] = $topic_name;
  }
  
  $form['topic'] = array(
    '#type' => 'select',
    '#options' => $options,
    '#title' => t('Which queue(s) to slot to'),
    '#description' => t('Enter which queue(s) you want the node to be slotted in.  (Note: This action does nothing if the node is already in the queue or if the node is unpublished.'),
    '#default_value' => isset($context['topic']) ? $context['topic'] : 'all',
    '#required' => TRUE,
  );
  return $form;
}

/**
 * Submit handler for configurable action to slot node to end of slot machine queue.
 */
function slot_machine_queue_action_submit($form, $form_state) {
  return array(
    'topic' => $form_state['values']['topic'],
  );
}

/**
 * Implementation of slot_machine_queue_action action
 */
function slot_machine_queue_action(&$node, $context = array()) {
  if (!$node->status) {
    return;
  }

  $topics = array();

  $vocab = variable_get('slot_machine_topics', 'none');
  $topic_names = array(0 => 'Home') + _sm_get_vocabulary_terms($vocab);
  
  // Find which topic queues to slot in
  if ($context['topic'] == 'all') {
    if (is_array($node->taxonomy[$vocab])) {
      foreach ($node->taxonomy[$vocab] as $tid) {
        if (is_numeric($tid)) {
          $topics[] = $tid;
        }
      }
    }
    else {
      if (isset($node->taxonomy[$vocab])) {
        if (is_numeric($node->taxonomy[$vocab])) {
          $topics[] = $node->taxonomy[$vocab];
        }
      }
    }
  }
  else {
    $topics[] = $context['topic'];
  }
  
  foreach ($topics as $topic) {
    
    // Don't slot the node if it's already queued for this topic.
    $queued = db_result(db_query("SELECT nid FROM {slot_machine} WHERE nid = %d AND topic = %d", $node->nid, $topic));
    if ($queued) {
      continue;
    }
    
    // Slot the item at the end of the queue for that topic and type
    db_query("INSERT INTO {slot_machine} (nid, topic, feature_type, priority) SELECT %d, %d, '%s', IF((SELECT priority FROM {slot_machine} WHERE topic = %d AND feature_type = '%s' LIMIT 1) IS NULL, 0, (SELECT priority + 1 FROM {slot_machine} WHERE topic = %d AND feature_type = '%s' ORDER BY priority DESC LIMIT 1))", $node->nid, $topic, $node->type, $topic, $node->type, $topic, $node->type);
          
    // If this is slotted at the front of the queue, it's published, so we need to mark the audit trail
    $priority = db_result(db_query("SELECT m.priority FROM {slot_machine} m INNER JOIN {slot_machine_slots} s ON m.priority = s.priority AND m.feature_type = s.feature_type WHERE m.nid = %d AND m.topic = %d AND m.feature_type = '%s'", $node->nid, $topic, $node->type));
    if (is_numeric($priority)) {
      db_query("UPDATE {slot_machine} SET schedule = %d WHERE nid = %d AND topic = %d AND feature_type = '%s'", time(), $node->nid, $topic, $node->type);
      db_query("INSERT INTO {slot_machine_history} (nid, topic, timestamp) VALUES (%d, %d, %d)", $node->nid, $topic, time());
        
      // Invoke the Live on Schedule trigger
      module_invoke_all('slot_machine', 'live', $node);
      
      drupal_set_message(t('%title has gone live on the schedule for the %channel channel.', array('%title' => $node->title, '%channel' => $topic_names[$topic])));
    }
    else {
      drupal_set_message(t('%title has been queued for the %channel channel.', array('%title' => $node->title, '%channel' => $topic_names[$topic])));
    }
  }
}
