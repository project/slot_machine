<?php
/**
 * Handler to sorting by slot weight
 */
class slot_machine_handler_sort_weight extends views_handler_sort {
  function query() {
    $this->ensure_my_table();
    
    $join = new views_join();
    $join->construct('slot_machine_slots', $this->table_alias, 'feature_type', 'feature_type');
    $join->extra = array(array('field' => 'priority', 'operator' => '=', 'value' => "$this->table_alias.priority", 'numeric' => 1));
    
    $this->slots_table = $this->query->ensure_table('sm_slots', $this->relationship, $join);
    // Add the field.
    $this->query->add_orderby($this->slots_table, $this->real_field, $this->options['order']);
  }
}
