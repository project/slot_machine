<?php

/**
 * @file slot_machine.views.inc
 * Provides support for the Views module.
 */

function slot_machine_views_handlers() {
  return array(
  'info' => array(
    'path' => drupal_get_path('module', 'slot_machine') .'/views',
    ),
    'handlers' => array(
      'slot_machine_handler_relationship' => array(
        'parent' => 'views_handler_relationship',
      ),
      'slot_machine_handler_filter_live' => array(
        'parent' => 'views_handler_filter_boolean_operator',
      ),
      'slot_machine_handler_filter_slot' => array(
        'parent' => 'views_handler_filter_in_operator',
      ),
      'slot_machine_handler_sort_weight' => array(
        'parent' => 'views_handler_sort',
      ),
    ),
  );
}

/**
 * Implementation of hook_views_data()
 */
function slot_machine_views_data() {
  // nodequeue_nodes table
  $data['slot_machine']['table']['group'] = t('Slot Machine');

  // slot
  $data['slot_machine']['slot'] = array(
    'title' => t('Slot'),
    'help' => t('Filter based on whether the node is live in the selected slot within the related queue.'),
    'filter' => array(
      'handler' => 'slot_machine_handler_filter_slot',
    ),
  );

  $data['slot_machine']['live'] = array(
    'title' => t('Live on schedule'),
    'help' => t('Filter based on whether the node is live on the schedule or not in the related queue.'),
    'filter' => array(
      'handler' => 'slot_machine_handler_filter_live',
      'label' => t('Live on schedule'),
    ),
  );

  $data['slot_machine']['weight'] = array(
    'title' => t('Weight'),
    'help' => t('Sort by the slot weight -- only works with nodes that are currently live in the schedule.'),
    'sort' => array(
      'handler' => 'slot_machine_handler_sort_weight',
    ),
  );

  $data['slot_machine']['priority'] = array(
    'title' => t('Priority'),
    'help' => t('Sort by priority -- mostly helpful when dealing with queued nodes.'),
    'sort' => array(
      'handler' => 'slot_machine_handler_sort_priority',
    ),
  );

  $data['slot_machine']['topic'] = array(
    'title' => t('Topic'),
    'help' => t('The Term ID argument allows users to filter a view by specifying a term ID (or 0 for "Home").'),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
  );

  $data['slot_machine']['feature_type'] = array(
    'title' => t('Feature Type'),
    'help' => t('The feature type argument allows users to filter a view by specifying a feature type (e.g., "story").'),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  return $data;
}

/**
 * Implementation of hook_views_data_alter().
 */
function slot_machine_views_data_alter(&$data) {
  // queue relationship
  $data['node']['slot_machine_rel'] = array(
    'group' => t('Slot Machine'),
    'title' => t('Queue'),
    'help' => t('Create a relationship to a Slot Machine slot or queue.'),
    'real field' => 'nid',
    'relationship' => array(
      'handler' => 'slot_machine_handler_relationship',
      'base' => 'slot_machine',
      'field' => 'nid',
      'label' => t('slot machine'),
    ),
  );
}
