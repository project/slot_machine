<?php

/**
 * Specialized relationship handler to add slot machine queue.
 */
class slot_machine_handler_relationship extends views_handler_relationship {
  function options(&$options) {
    parent::options($options);

    $options['limit'] = FALSE;
    $options['topic'] = '';
    $options['feature_type'] = '';
  }

  /**
   * Default options form that provides the label widget that all fields
   * should have.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['limit'] = array(
      '#type' => 'checkbox',
      '#title' => t('Limit by topic and feature type (check this unless you are passing these in by arguments)'),
      '#default_value'=> $this->options['limit'],
    );

    $vocab = variable_get('slot_machine_topics', 'none');
    if (variable_get('slot_machine_home_topic', 1)) {
      $options = array(0 => 'Home');
    }
    else {
      $options = array();
    }
    $options += _sm_get_vocabulary_terms($vocab);

    $form['topic'] = array(
      '#prefix' => '<div><div id="edit-options-sm">',
      '#type' => 'select',
      '#title' => t('Topic'),
      '#options' => $options,
      '#default_value' => $this->options['topic'],
      '#process' => array('expand_checkboxes', 'views_process_dependency'),
      '#dependency' => array('edit-options-limit' => array(TRUE)),
    );

    $options = array(0 => 'All');
    $content_types = _slot_machine_get_content_types();
    $feature_types = db_query('SELECT feature_type FROM {slot_machine_slots}');
    while ($obj = db_fetch_object($feature_types)) {
      $options[$obj->feature_type] = $content_types[$obj->feature_type];
    }

    $form['feature_type'] = array(
      '#suffix' => '</div></div>',
      '#type' => 'select',
      '#title' => t('Feature Type'),
      '#options' => $options,
      '#default_value'=> $this->options['feature_type'],
      '#process' => array('expand_checkboxes', 'views_process_dependency'),
      '#dependency' => array('edit-options-limit' => array(TRUE)),
    );
  }

  /**
   * Called to implement a relationship in a query.
   */
  function query() {
    // Figure out what base table this relationship brings to the party.
    $join = new views_join();
    $join->definition = array(
      'table' => 'slot_machine',
      'field' => 'nid',
      'left_table' => 'node',
      'left_field' => 'nid',
    );

    if (!empty($this->options['required'])) {
      $join->definition['type'] = 'INNER';
    }

    if (!empty($this->options['limit'])) {
      $join->definition['extra'] = array(
        array(
          'field' => 'topic',
          'value' => $this->options['topic'],
          'numeric' => TRUE,
        ),
      );
        
      if ($this->options['feature_type']) {
        $join->definition['extra'][] = array(
          'field' => 'feature_type',
          'value' => $this->options['feature_type'],
          'numeric' => FALSE,
        );
      }
    }

    $join->construct();

    $alias = $join->definition['table'] . '_' . $join->definition['left_table'];

    $this->alias = $this->query->add_relationship($alias, $join, 'slot_machine', $this->relationship);
  }
}