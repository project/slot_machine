<?php
/**
 * Handler to filter for nodes that are live in the schedule or not
 */
class slot_machine_handler_filter_live extends views_handler_filter_boolean_operator {
  function query() {
    $this->ensure_my_table();
    
    // join slot_machine_slots table
    $join = new views_join();
    $join->construct('slot_machine_slots', $this->table_alias, 'feature_type', 'feature_type');
    $join->extra = array(array('field' => 'priority', 'operator' => '=', 'value' => "$this->table_alias.priority", 'numeric' => 1));
    
    $this->slots_table = $this->query->ensure_table('sm_slots', $this->relationship, $join);

    $this->query->add_where($this->options['group'], "$this->slots_table.feature_type ". ($this->value ? 'IS NOT NULL' : 'IS NULL'));    
  }
}
