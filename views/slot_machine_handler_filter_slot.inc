<?php

/**
 * Filter by slot
 */
class slot_machine_handler_filter_slot extends views_handler_filter_in_operator {
  function get_value_options() {
    if (isset($this->value_options)) {
      return;
    }

    $this->value_options = array();
    $slots = db_query('SELECT sid, name FROM {slot_machine_slots}');
    while ($obj = db_fetch_object($slots)) {
      $this->value_options[$obj->sid] = check_plain($obj->name);
    }
  }

  function operator_options() {
    return array(
      'in' => t('Is one of'),
    );
  }

  function query() {
    if (empty($this->value)) {
      return;
    }
    $this->ensure_my_table();

    // join slot_machine_slots table
    $join = new views_join();
    $join->construct('slot_machine_slots', $this->table_alias, 'feature_type', 'feature_type');
    
    $this->slots_table = $this->query->ensure_table('sm_slots', $this->relationship, $join);

    $replace = array_fill(0, sizeof($this->value), '%d');
    $in = ' (' . implode(", ", $replace) . ')';
    
    $this->query->add_where($this->options['group'], "$this->table_alias.priority = $this->slots_table.priority AND $this->slots_table.sid " . $this->operator . $in, array_values($this->value));    
  }
}
