function fcSlotThisUIAttach() { 

  /********************************************************************************
   * "Slot This" button jQuery
   ********************************************************************************/

  // Disable all checked boxes, so that users can't unslot already slotted items
  $("form#slot-machine-slot-this-form input:checked").each(function() {
    this.disabled = true;
  });

  // Display proper popup if "Slot This" button is clicked, and cancel all other open popups
  $('button.slot-this-button').click(function() {
    var nid = this.name;
    if ($('#' + nid + '_slot_popup').css('display') == 'none') {
      $('button.slot-this-button').each(function()  {
        if ($('#' + this.name + '_slot_popup').css('display') != 'none') {
          cancel_slot_this_popup(this.name);
        }
      });
      $('#' + nid + '_slot_popup').show('fast');
    }
    else {
      cancel_slot_this_popup(nid);
    }
    return false;
  });

  // If the OK button is pressed on the "Slot This" popup, disable checked boxes 
  // and submit via AJAX
  $("form#slot-machine-slot-this-form button.slot-popup-ok").click(function() {
    var nid = this.name;
    var topics = '';
    for (i = 0; i < Drupal.settings.topics.length; i++) {
      $('#edit-' + nid + '-' + Drupal.settings.topics[i] + ', #edit-' + nid + '-home-' + Drupal.settings.topics[i]).each(function() {
        if (this.checked == true) {
          if (!this.disabled) {
            topics += (this.value == 'home_0' ? 0 : this.value) + ','; 
          }
          this.disabled = true;
        }
      });
    }

    // save the checked options via AJAX
    if (topics.length > 0) {
      var uri = '/slot_this/' + nid + '/' + topics;
      $.get(uri, null, null);
    }

    $('#' + nid + '_slot_popup').hide('fast');
    return false;
  });

  // If the Cancel button is pressed on the "Slot This" popup, uncheck those boxes that were
  // checked during this pop-up session.
  $("form#slot-machine-slot-this-form button.slot-popup-cancel").click(function() {
    var nid = this.name;
    cancel_slot_this_popup(nid);
    return false;
  });
}

function cancel_slot_this_popup(nid) {
  for (i = 0; i < Drupal.settings.topics.length; i++) {
    $('#edit-' + nid + '-' + Drupal.settings.topics[i] + ', #edit-' + nid + '-home-' + Drupal.settings.topics[i]).each(function() {
      if (this.disabled == false) {
        this.checked = false;
      }
    });
  }
  $('#' + nid + '_slot_popup').hide('fast');
}
  
if (Drupal.jsEnabled) {
  $(document).ready(fcSlotThisUIAttach);
}