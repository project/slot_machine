function fcSlotMachineUIAttach() { 

  /********************************************************************************
   * Featured Content Queue jQuery
   ********************************************************************************/

  // Disable all checked boxes, so that users can't unslot already slotted items
  $("form#slot-machine-content-queue-form input:checked").each(function() {
    this.disabled = true;
  });
  
  // Fill for each item the list of slotted topics
  $('a.choose').each(function() {
    refreshSlotList(this.name);
  });

  // If slot all is clicked, check all relevant checkboxes  
  $("a.slot-all").click(function() {
    $("span.throbber").addClass("throbbing");
    setTimeout("perform_select_all()", 1);
    return false;
  });

  // Automatically submit topic/type dropdowns
  $('#edit-topic, #edit-type').change(function() {
    this.form.submit();
  });
  
  // Do a select all on the row where the + icon was clicked.
  $('a.item-select-all').click(function() {
    var nid = this.name;
    $('#' + nid + '_slot_popup').find("input[@type='checkbox']").each(function() {
      if (this.value.substr(0, 7) == 'default') {
        this.checked = true;
      }
      if ($('#edit-include-home').attr('checked')) {
        if (this.value.substr(0, 4) == 'home') {
          this.checked = true;
        }
      }
    });
    refreshSlotList(nid);
    return false;
  });
  
  // Display proper popup if "Choose" link is clicked, and cancel all other popups
  $('a.choose').click(function() {
    var nid = this.name;
    if ($('#' + nid + '_slot_popup').css('display') == 'none') {
      $('a.choose').each(function()  {
        if ($('#' + this.name + '_slot_popup').css('display') != 'none') {
          cancel_queue_popup(this.name);
        }
      });
      $('#' + nid + '_slot_popup').show('fast');
    }
    else {
      cancel_queue_popup(nid);
    }
    return false;
  });
  
  // If the OK button is pressed on the "Choose" popup, refresh the slotted topics list 
  // and the check the choose box if necessary
  $("form#slot-machine-content-queue-form button.slot-popup-ok").click(function() {
    var nid = this.name;
    refreshSlotList(nid);
    $('#' + nid + '_slot_popup').hide('fast');
    return false;
  });
  
  // If the Cancel button is pressed on the "Choose" popup, uncheck those boxes that were
  // checked during this pop-up session.  (We can infer this from the slot list.)
  $("form#slot-machine-content-queue-form button.slot-popup-cancel").click(function() {
    var nid = this.name;
    cancel_queue_popup(nid);
    return false;
  });

  /********************************************************************************
   * Slot Machine Schedule jQuery
   ********************************************************************************/
  // right at submit, un-disable everything so it will get submitted properly 
  $("form#slot-machine-view-schedule-topic-form .form-select").change(function() {
    if (this.value != $(this).attr('current_topic')) {
      this.form.submit();
    }
  });
  
  // Display Rotate checkbox if "Remove" is checked and available
  $("input.remove").click(function() {
    var sid = this.id.substr(12);
    if (this.checked) {
      $("div#rot-checkbox-" + sid).show();
    }
    else {
      $("div#rot-checkbox-" + sid).hide();
      $("input#edit-rot-" + sid).attr('checked', false);
      if (typeof(Drupal.settings.rotations[sid]) != 'undefined') {
        for (i = 0; i < Drupal.settings.rotations[sid].length; i++) {
          if ($("input#edit-remove-" + Drupal.settings.rotations[sid][i]).attr('disabled') == true) {
            $("input#edit-remove-" + Drupal.settings.rotations[sid][i]).attr('checked', false);
            $("input#edit-remove-" + Drupal.settings.rotations[sid][i]).attr('disabled', false);
            $("input#edit-rot-" + Drupal.settings.rotations[sid][i]).attr('checked', false);
            $("input#edit-rot-" + Drupal.settings.rotations[sid][i]).attr('disabled', false);
            $("div#rot-checkbox-" + Drupal.settings.rotations[sid][i]).hide();
          }
        }
      }
    }
  });
  
  // If Rotate checkbox is checked, automatically check the remove box for slots being rotated to
  $("input.rotate").click(function() {
    var sid = this.id.substr(9);
    if (this.checked) {
      for (i = 0; i < Drupal.settings.rotations[sid].length; i++) {
        $("input#edit-remove-" + Drupal.settings.rotations[sid][i]).attr('checked', true);
        $("input#edit-remove-" + Drupal.settings.rotations[sid][i]).attr('disabled', true);
        $("input#edit-rot-" + Drupal.settings.rotations[sid][i]).attr('checked', true);
        $("input#edit-rot-" + Drupal.settings.rotations[sid][i]).attr('disabled', true);
        $("div#rot-checkbox-" + Drupal.settings.rotations[sid][i]).show();
      }
    }
    else {
      for (i = 0; i < Drupal.settings.rotations[sid].length; i++) {
        $("input#edit-remove-" + Drupal.settings.rotations[sid][i]).attr('checked', false);
        $("input#edit-remove-" + Drupal.settings.rotations[sid][i]).attr('disabled', false);
        $("input#edit-rot-" + Drupal.settings.rotations[sid][i]).attr('checked', false);
        $("input#edit-rot-" + Drupal.settings.rotations[sid][i]).attr('disabled', false);
        $("div#rot-checkbox-" + Drupal.settings.rotations[sid][i]).hide();
      }
    }  
  });
  
  // If schedule Reset button is checked, uncheck all Remove boxes, the Rotate box, and default the frequencies
  $schedule_form = $("#slot-machine-schedule-form-1");
  $("#edit-reset", $schedule_form).click(function() {
    $("div#rot-checkbox").hide();
    $("input[@type='checkbox']", $schedule_form).each(function () {
      this.checked = false;
      this.disabled = false;
    });
    $("input#edit-remove-6").attr('disabled', false);
    $("select", $schedule_form).each(function() {
      this.value = $(this).attr('original');
    });
    return false;
  });

  // If Preview button is clicked, undisable everything so they will get submitted
  $("#edit-preview", $schedule_form).click(function() {
    $("input", $schedule_form).each(function() {
      $(this).attr('disabled', false);
    });
  });

  // If select all is clicked in queue, check all relevant checkboxes  
  $("a.queue-select-all").click(function(){
    $("form#slot-machine-queue-form input[@type='checkbox']").each(function () {
      this.checked = true;
    });
    return false;
  });

  // If queue Reset button is clicked, uncheck all Remove boxes and default the queue values
  $("#edit-reset-queue").click(function() {
    $("form#slot-machine-queue-form input[@type='text']").each(function() {
      this.value = $(this).attr('original');
    });
    $("form#slot-machine-queue-form input[@type='checkbox']").each(function () {
      this.checked = false;
    });
    return false;
  });

  // Display confirmation popup if Reorder is clicked and items have been selected for removal
  $("#edit-reorder").click(function() {
    var remove = false;
    $("form#slot-machine-queue-form input[@type='checkbox']").each(function () {
      if (this.checked) {
        remove = true;
      }
    });
    if (remove) {
      $("#confirmation-popup").show('fast');
      return false;
    }
  });
  
  // Handle cancel button on confirmation popup
  $("button#confirmation-cancel").click(function() {
    $("#confirmation-popup").hide('fast');
    return false;
  });
}

/********************************************************************************
 * Helper functions
 ********************************************************************************/

function refreshSlotList(nid) {
  var slot_html = '';
  for (i = 0; i < Drupal.settings.topics.length; i++) {
    $('#edit-cb' + nid + '-' + Drupal.settings.topics[i] + ', #edit-cb' + nid + '-default-' + Drupal.settings.topics[i]+ ', #edit-cb' + nid + '-home-' + Drupal.settings.topics[i]).each(function() {
      if (this.checked) {
        slot_html += $(this).parent().text();
        if (this.disabled == false) {
          slot_html += '&nbsp;<a class="remove-slot" name="' + nid + '" id="remove-' + this.id + '" href="#remove">x</a>';
        }
        slot_html += '<br/>';
      }
    });
  }
  $('#' + nid + '_slots').html(slot_html);
  
  // Uncheck item if trash icon is clicked
  $("a.remove-slot").click(function() {
    $("#" + this.id.substr(7)).attr('checked', false);
    refreshSlotList(this.name);
    return false;
  });
  return slot_html;
}

function perform_select_all() {
  $("form#slot-machine-content-queue-form input[@type='checkbox']").each(function() {
    if (this.value.substr(0, 7) == 'default') {
      this.checked = true;
    }
    if ($('#edit-include-home').attr('checked')) {
      if (this.value.substr(0, 4) == 'home') {
        this.checked = true;
      }
    }
  });
  $('a.choose').each(function()  {
    refreshSlotList(this.name);
  });
  $("span.throbber").removeClass("throbbing");  
}

function cancel_queue_popup(nid) {
  var slot_list = $('#' + nid + '_slots').text();
  for (i = 0; i < Drupal.settings.topics.length; i++) {
    $('#edit-cb' + nid + '-' + Drupal.settings.topics[i] + ', #edit-cb' + nid + '-default-' + Drupal.settings.topics[i] + ', #edit-cb' + nid + '-home-' + Drupal.settings.topics[i]).each(function() {
      if (slot_list.indexOf(trim($(this).parent().text())) == -1) {
        this.checked = false;
      }
      else {
        this.checked = true;
      }
    });
  }
  $('#' + nid + '_slot_popup').hide('fast');
}

function trim(str) {
  return str.replace(/^\s+/, '').replace(/\s+$/, '');
}

if (Drupal.jsEnabled) {
  $(document).ready(fcSlotMachineUIAttach);
}
