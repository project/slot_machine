*******************************************************************************

slot_machine

Description:
-------------------------------------------------------------------------------

  This module generalises the original slot machine module created for
  FastCompany.com, built by Marco Carbone of Advomatic and sponsored by
  Fast Company/Mansueto.


Installation & Use:
-------------------------------------------------------------------------------

  1.  Enable module in module list located at administer > build > modules.
  2.  Create a vocabulary to use as your slot_machine 'topics' (or channel)
  3.  Go to admin/settings/slot_machine and set the topics vocabulary
  4.  Create feature types and frequency settings as required.
  5.  Go to admin/content/slotqueue to slot nodes into topics.  You can also
      use the "Slot This" button displayed on full nodes.  NOTE: Admins will 
      need javascript enabled to administrate slot_machine.
  6.  Go to admin/content/slotmachine to view the current schedule and set
      frequencies for slots in particular topics.
  7.  Configure cron.php to run at least once an hour.  IMPORTANT: If cron is
      running less often than this, the scheduler may not function properly.
  8.  Use the channel paths configured in the settings area to display content. 
      You can also use Views or the API function: 
      
        slot_machine_get_slot_nid($topic, $feature_name)


FAQ:
-------------------------------------------------------------------------------

  ***Featured Content Queue page***

    1. What does the Reset button below the Topic and Feature Type popups do? 

    It resets all the filters. So if you've filtered by Type, by Topic, and
    even additionally a keyword, and then you  press "Reset," you'll get the
    default version of that page, unfiltered and in reverse chron.

    2. Clicking "Slot All" doesn't seem to do anything. 

    This is a shortcut for slotting items, but it is mostly useful if you are
    using Topics. What it does is: if a node is tagged with a Topic, the slot
    all button will slot every item on that page in the queue for its Topic.
    If you check the "Include Home in 'slot all' and '+' actions" box and then
    click "Slot All," all items on the page will be slotted for the Home queue
    as well.

    3. What do the small "+" links do? 

    Same thing as 'slot all' as described above, except for that specific node.

  ***Slot Machine Schedule page***

    1. It's not obvious what the upper Reset button does - can't see anything
    change when pressing it. 

    It resets any changes you've made to what you had initially -- a "return to
    defaults" sort of thing. So if you were to change some frequencies and
    check some boxes, and then click "Reset," they will return to their initial
    values.

    2. What is the Preview button for? I have two items in the queue but
    pressing the Preview button just shows me the same information but only for
    the currently live node. 

    It previews any manual changes you are making to the schedule. So, for
    example, if you check the remove box and then click "Preview," you will see
    what your schedule will look like when the Remove goes through. Very useful
    for when you are removing several items at once, especially when rotation
    is enabled.


Authors:
-------------------------------------------------------------------------------

  --Marco Carbone
    http://drupal.org/user/68488
  --Katherine Bailey
    http://drupal.org/user/172987


TODO
-------------------------------------------------------------------------------

  + there are some TODO comments in slot_machine.module when handling Feature 
    Type edits by the admin
  + edge-case testing
  + maybe we should make priorities modifiable and generally less obscure?
