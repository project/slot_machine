<?php

/********************************************************************************
 * ADMINISTRATION FUNCTIONS
 ********************************************************************************/
function slot_machine_settings() {
  // need this for changing topics_path
  menu_rebuild();

  $form = array();
  $vocabularies = _slot_machine_get_vocabs();

  $form['slot_machine_topics'] = array(
    '#type' => 'select',
    '#title' => t('Topics Vocabulary'),
    '#options' => array('none' => '--select--') + $vocabularies,
    '#default_value' => variable_get('slot_machine_topics', 'none'),
    '#description' => t('Choose which vocabulary to use for your Slot Machine pages.'),
  );
  
  $form['slot_machine_include_child_terms'] = array(
    '#type' => 'checkbox',
    '#title' => 'Include child terms as separate topics?',
    '#default_value' => variable_get('slot_machine_include_child_terms', 0),
    '#description' => t('This option is only relevant if you choose a hierarchical vocabulary as your topics vocabulary'),
  );
  
  $form['slot_machine_home_topic'] = array(
    '#type' => 'checkbox',
    '#title' => 'Treat homepage as a separate topic?',
    '#default_value' => variable_get('slot_machine_home_topic', 1),
    '#description' => t('If unchecked, you can use your topics path with no topic passed in to have a homepage showing the top slot from each topic.')
  );
  
  $example_channels = '';
  $topics_path = variable_get('slot_machine_topics_path', 'slot_machine');
  if (variable_get('slot_machine_topics', 'none') != 'none') {
    $example_channels = ' '. t('Current channels') .':<ul><li>'. l($topics_path, $topics_path) .'</li>';
    $topics = _sm_get_vocabulary_terms(variable_get('slot_machine_topics', 'none'));
    foreach ($topics as $topic) {
      $example_channels .= '<li>'. l($topics_path .'/'. $topic, $topics_path .'/'. $topic) .'</li>';
    }
    $example_channels .= '</ul>';
  }
  $form['slot_machine_topics_path'] = array(
    '#type' => 'textfield',
    '#title' => 'Channels path',
    '#default_value' => $topics_path,
    '#description' => t('Choose the path you would like to use to display your channels.') . $example_channels,
  );

  return system_settings_form($form);
}

function slot_machine_rotation_settings() {
  $form['help'] = array(
    '#type' => 'markup',
    '#value' => t('Select the types for which you want to enable content rotation.  You can enable rotation only for types that have more than one associated slot.<br/>'),
  );

  $rotatable_types = array();
  $results = db_query("SELECT feature_type, COUNT(1) FROM {slot_machine_slots} GROUP BY feature_type HAVING COUNT(1) > 1");
  while ($obj = db_fetch_object($results)) {
    $rotatable_types[$obj->feature_type] = $obj->feature_type;
  }
  if (count($rotatable_types)) {
    $form['slot_machine_enable_content_rotation'] = array(
      '#type' => 'checkboxes',
      '#title' => 'Rotatable types',
      '#options' => $rotatable_types,
      '#default_value' => variable_get('slot_machine_enable_content_rotation', array()),
    );
  }
  else {
    drupal_set_message(t('You currently have no types with multiple slots.'));
  }
  return system_settings_form($form);
}

function _slot_machine_get_vocabs() {
  $vocabularies = taxonomy_get_vocabularies();
  $vocs = array();
  foreach ($vocabularies as $vid => $voc) {
    $vocs[$vid] = $voc->name;
  }
  return $vocs;
}

function slot_machine_add_frequency() {
  $times_options = _slot_machine_get_times_options();
  $output = drupal_get_form('slot_machine_frequency_form');
  $output .= '<h2>Existing frequency settings</h2>';
  $result = db_query('SELECT fid, name, frequency, start FROM {slot_machine_frequencies}');
  $header = array(t('Frequency Name'), t('Period'), t('Delete'));
  $rows = array();
  while ($row = db_fetch_object($result)) {
    $period = ($row->frequency ? 'Every '. $row->frequency / 3600 .' hours' : 'Daily at '. $times_options[substr($row->start, 0, 5)]);
    $tablerow = array(
      array('data' => $row->name . ($row->name != 'Never' ? ' ('. l('edit', 'admin/settings/slot_machine/frequency/edit/'. $row->fid) .')' : '')),
      array('data' => ($row->name != 'Never' ? $period : 0)),
      array('data' => ($row->name != 'Never' ? l(t('Delete'), 'admin/settings/slot_machine/frequency/delete/'. $row->fid) : 'N/A')),
    );
    $rows[] = $tablerow;
  }
  $output .= theme('table', $header, $rows, array('id' => 'slotmachine_frequencies'));
  return $output;
}

function slot_machine_frequency_form($form_state, $defaults = NULL) {
  $form = array();
  $form['frequency_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Frequency Name'),
    '#default_value' => ($defaults['name'] ? $defaults['name'] : ''),
    '#description' => t('For example, "Daily" or "Twice a Day".'),
  );
  $interval_options = array(
    '1' => '1',
    '2' => '2',
    '3' => '3',
    '4' => '4',
    '5' => '5',
    '6' => '6',
    '7' => '7',
    '8' => '8',
    '9' => '9',
    '10' => '10',
    '11' => '11',
    '12' => '12',
    '24' => '24',
    '48' => '48',
    '72' => '72',
    '96' => '96',
    '120' => '120',
    '144' => '144',
    '168' => '168',
  );
  $form['interval'] = array(
    '#type' => 'select',
	'#prefix' => t('Update every '),
    '#options' => array('0' => 'select') + $interval_options,
    '#default_value' => ($defaults['frequency'] ? $defaults['frequency'] : '0'),
	'#suffix' => t(' hours'),
  );
  $form['or'] = array(
	'#type' => 'markup',
	'#value' => '<div class="bold"> - OR - </div>',
  );
  $form['daily_time'] = array(
    '#type' => 'select',
    '#prefix' => t('Daily at '),
    '#options' => _slot_machine_get_times_options(),
    '#default_value' => ($defaults['start'] ? $defaults['start'] : '12:00'),
  );
  if ($defaults['formtype'] == 'edit') {
    $form['fid'] = array(
      '#type' => 'hidden',
      '#value' => $defaults['fid'],
    );
  }
  $form['cron_warning'] = array(
    '#prefix' => '<div>',
    '#value' => t('Please note that cron must run at least as frequently as your most frequent setting in order for your scheduling to work properly.'),
    '#suffix' => '</div>',
  );
  $form['submit'] = array(
  '#type' => 'submit',
  '#value' => t('Save'),
  );
  return $form;
}

function slot_machine_frequency_form_submit($form, &$form_state) {
  if (!$form_state['values']['fid']) {
    if ($form_state['values']['interval'] != '0') {
      $interval = $form_state['values']['interval'] * 3600;
      db_query('INSERT INTO {slot_machine_frequencies} (name, frequency, start) VALUES ("%s", %d, NULL)', $form_state['values']['frequency_name'], $interval);
    }
    else {
      db_query('INSERT INTO {slot_machine_frequencies} (name, frequency, start) VALUES ("%s", NULL, "%s")', $form_state['values']['frequency_name'], $form_state['values']['daily_time']);
    }
  }
  else {
    $interval = ($form_state['values']['interval'] == '0' ? NULL : $form_state['values']['interval'] * 3600);
    if ($interval) {
      db_query('UPDATE {slot_machine_frequencies} SET name = "%s", frequency = "%s", start = NULL WHERE fid = %d', $form_state['values']['frequency_name'], $interval, $form_state['values']['fid']);
    }
    else {
      $start = $form_state['values']['daily_time'];
      db_query('UPDATE {slot_machine_frequencies} SET name = "%s", frequency = NULL, start = "%s" WHERE fid = %d', $form_state['values']['frequency_name'], $start, $form_state['values']['fid']);
    }
  }
  $form_state['redirect'] = 'admin/settings/slot_machine/add_frequency';
}

function slot_machine_edit_frequency($fid) {
  $frequency = db_fetch_object(db_query('SELECT name, frequency, start FROM {slot_machine_frequencies} WHERE fid=%d', $fid));
  
  if ($frequency->name == 'Never') {
    drupal_set_message('You cannot edit the "Never" frequency.');
    drupal_goto('admin/settings/slot_machine/add_frequency');
  }
  
  $defaults = array();
  $defaults['fid'] = $fid;
  $defaults['name'] = $frequency->name;
  if ($frequency->frequency != NULL) {
    $interval_hours = $frequency->frequency/3600;
    $defaults['frequency'] = $interval_hours;
  }
  if ($frequency->start != NULL) {
    list($h,$m,$s) = split(":", $frequency->start);
    $defaults['start'] = $h .':'. $m;
  }
  $defaults['formtype'] = 'edit';
  return drupal_get_form('slot_machine_frequency_form', $defaults);
}

function slot_machine_delete_frequency(&$form_state, $fid) {
  $frequency_name = db_result(db_query('SELECT name FROM {slot_machine_frequencies} WHERE fid = %d', $fid));

  if ($frequency_name == 'Never') {
    drupal_set_message('You cannot delete the "Never" frequency.');
    drupal_goto('admin/settings/slot_machine/add_frequency');
  }

  $form['fid'] = array('#type' => 'hidden', '#value' => $fid);
  $form['freq_name'] = array('#type' => 'hidden', '#value' => $frequency_name);
  return confirm_form($form, t('Are you sure you want to delete the frequency %name?', array('%name' => $frequency_name)), 'admin/settings/slot_machine/add_frequency', '', t('Delete'), t('Cancel'));
}

function slot_machine_delete_frequency_submit($form, &$form_state) {
  db_query('DELETE FROM {slot_machine_frequencies} WHERE fid = %d', $form_state['values']['fid']);
  db_query('UPDATE {slot_machine_slots_frequencies} SET fid = 1 WHERE fid = %d', $form_state['values']['fid']);
  drupal_set_message(t('The frequency %name has been removed.', array('%name' => $form_state['values']['freq_name'])));
  cache_clear_all();
  $form_state['redirect'] = 'admin/settings/slot_machine/add_frequency';
};

function slot_machine_add_featuretype() {
  $output = drupal_get_form('slot_machine_featuretype_form');
  $output .= '<h2>Existing Feature Types</h2>';
  $result = db_query('SELECT sid, name, feature_type, weight FROM {slot_machine_slots} ORDER BY weight ASC');
  $header = array(t('Slot Name'), t('Content Type'), t('Weight'), t('Delete'));
  $rows = array();
  while ($row = db_fetch_object($result)) {
    $tablerow = array(
    array('data' => $row->name .' ('. l('edit', 'admin/settings/slot_machine/slot/edit/'. $row->sid) .')'),
    array('data' => $row->feature_type),
    array('data' => $row->weight),
    array('data' => l(t('Delete'), 'admin/settings/slot_machine/slot/delete/'. $row->sid)),
    );
    $rows[] = $tablerow;
  }
  $output .= theme('table', $header, $rows, array('id' => 'slotmachine_slots'));
  return $output;
}

function slot_machine_featuretype_form($form_state, $defaults = NULL) {
  $form = array();
  $form['feature_type_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Feature Type'),
    '#default_value' => ($defaults['name'] ? $defaults['name'] : ''),
    '#description' => t('Enter a name for your new feature type'),
  );
  $content_types = _slot_machine_get_content_types();

  $form['feature_type_type'] = array(
    '#type' => 'select',
    '#title' => 'Content type',
    '#options' => $content_types,
    '#disabled' => $defaults['formtype'] == 'edit' ? TRUE : FALSE,
    '#description' => $defaults['formtype'] == 'edit' ? t('For the meantime, you cannot edit a slot\'s content type.') : '',
    '#default_value' => ($defaults['content_type'] ? $defaults['content_type'] : ''),
  );
  $form['feature_type_weight'] = array(
    '#type' => 'textfield',
    '#title' => t('Weight'),
    '#default_value' => (is_numeric($defaults['weight']) ? $defaults['weight'] : ''),
    '#description' => t('The weight determines the order of appearance in the slot machine and on the channels.'),
  );
  if ($defaults['formtype'] == 'edit') {
    $form['sid'] = array(
      '#type' => 'hidden',
      '#value' => $defaults['sid'],
    );
  }
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  return $form;
}

function slot_machine_featuretype_form_submit($form, &$form_state) {
  if (!$form_state['values']['sid']) {
    $priority = _slot_machine_next_priority($form_state['values']['feature_type_type']);
    db_query('INSERT INTO {slot_machine_slots} (name, feature_type, priority, weight) VALUES ("%s", "%s", %d, %d)', $form_state['values']['feature_type_name'], $form_state['values']['feature_type_type'], $priority, $form_state['values']['feature_type_weight']);
  } 
  else {
    db_query('UPDATE {slot_machine_slots} SET name = "%s", feature_type = "%s", weight = %d WHERE sid = %d', $form_state['values']['feature_type_name'], $form_state['values']['feature_type_type'], $form_state['values']['feature_type_weight'], $form_state['values']['sid']);
    // TODO: Enable content type on edit, but we have to make sure that priorities are unique with 
    // no gaps for both the original and new types
  }
  // TODO: any new content that gets published as a result of these slots being added need to have
  // their publish date and history updated
  $form_state['redirect'] = 'admin/settings/slot_machine/add_featuretype';
}

function slot_machine_edit_featuretype($sid) {
  $feature_type = db_fetch_object(db_query('SELECT name, feature_type, weight FROM {slot_machine_slots} WHERE sid = %d', $sid));
  $defaults = array(
    'sid' => $sid,
    'name' => $feature_type->name,
    'content_type' => $feature_type->feature_type,
    'weight' => $feature_type->weight,
    'formtype' => 'edit',
  );
  return drupal_get_form('slot_machine_featuretype_form', $defaults);
}

function slot_machine_delete_featuretype(&$form_state, $sid) {
  $featuretype_name = db_result(db_query('SELECT name FROM {slot_machine_slots} WHERE sid = %d', $sid));
  $form['sid'] = array('#type' => 'hidden', '#value' => $sid);
  $form['ft_name'] = array('#type' => 'hidden', '#value' => $featuretype_name);
  return confirm_form($form, t('Are you sure you want to delete the feature type %name?', array('%name' => $featuretype_name)), 'admin/settings/slot_machine/add_featuretype', '', t('Delete'), t('Cancel'));
}

function slot_machine_delete_featuretype_submit($form, &$form_state) {
  db_query('DELETE FROM {slot_machine_slots} WHERE sid = %d', $form_state['values']['sid']);
  db_query('DELETE FROM {slot_machine_slots_frequencies} WHERE sid = %d', $form_state['values']['sid']);
  drupal_set_message(t('The feature type %name has been removed.', array('%name' => $form_state['values']['ft_name'])));
  cache_clear_all();
  $form_state['redirect'] = 'admin/settings/slot_machine/add_featuretype';
};


function _slot_machine_next_priority($content_type) {
  $last_priority = db_result(db_query('SELECT priority FROM {slot_machine_slots} WHERE feature_type="%s" ORDER BY priority DESC LIMIT 0,1', $content_type));
  if ($last_priority === FALSE) {
    return 0;
  }
  else {
    return $last_priority + 1;
  }
}

function _slot_machine_get_times_options() {
  return array(
    '00:00' => '12am',
    '01:00' => '1am',
    '02:00' => '2am',
    '03:00' => '3am',
    '04:00' => '4am',
    '05:00' => '5am',
    '06:00' => '6am',
    '07:00' => '7am',
    '08:00' => '8am',
    '09:00' => '9am',
    '10:00' => '10am',
    '11:00' => '11am',
    '12:00' => '12pm',
    '13:00' => '1pm',
    '14:00' => '2pm',
    '15:00' => '3pm',
    '16:00' => '4pm',
    '17:00' => '5pm',
    '18:00' => '6pm',
    '19:00' => '7pm',
    '20:00' => '8pm',
    '21:00' => '9pm',
    '22:00' => '10pm',
    '23:00' => '11pm',
  );
}